# Docker Traefik Reverse Proxy
A Docker setup to easily deploy a traefik reverse proxy container for local use.
Works in conjunction with a [traefik webserver](https://bitbucket.org/QuentinBuiteman/docker-traefik-webserver.git) for each project.

## Setup
Add this project to a folder on your server.
```
git clone https://bitbucket.org/QuentinBuiteman/docker-traefik.git traefik
```
Navigate to the created folder to create your .env file.
```
cd traefik
cp .env.example .env
```
Change the env file to your liking. You can set the traefik version you'd like to use (only v2 and higher supported).

## Setup network
Create an external Docker network, which all containers will communicate with.
```
docker network create proxy
```

### Start the containers
```
docker-compose up -d
```

## Supported PHP versions
* 5.5
* 5.6
* 7.0
* 7.1
* 7.2
* 7.3
